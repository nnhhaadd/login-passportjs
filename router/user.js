const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const passport = require('passport');

router.post('/login', function(req, res, next){
  passport.authenticate('local', {
    successRedirect: '/user/personal',
    failureRedirect: '/',
    failureFlash: true
  })(req, res, next)
})

router.post('/register', function(req, res){
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;

  User.findOne({'username': username}, function(err, user){
    if(err){
      req.flash('danger',err);
      res.redirect('/');
    }else{
      if(user){
        req.flash('danger', 'username has been taken');
        res.redirect('/');
      }else{
        req.checkBody('username', 'username is require').notEmpty();
        req.checkBody('email', 'email is not valid').isEmail();
        req.checkBody('password', 'password is require').notEmpty();
        let errors = [];
        for (var i = 0; i < req.validationErrors().length; i++) {
          errors.push(req.validationErrors()[i].msg);
        }
        if(errors.length >0){
          req.flash('danger', errors);
          res.redirect('/');
        }else{
          let newUser =  new User({
            username: username,
            email: email,
            password: password
          });
          bcrypt.genSalt(10, function(err, salt){
            if(err){
              req.flash('danger', err);
              res.redirect('/');
            }else {
              bcrypt.hash(newUser.password, salt, function(err, hash){
                if(err){
                  req.flash('danger',err);
                  res.redirect('/');
                }else{
                  newUser.password = hash;
                  newUser.save(function(err){
                    if(err){
                      req.flash('danger',err);
                      res.redirect('/');
                    }else{
                      req.flash('success','New account has been created');
                      res.redirect('/')
                    }
                  })
                }
              })
            }
          })
        }
      }
    }
  })
})

router.get('/personal', function(req, res){
  if(req.user){
    res.render('personal',{title: 'USER'});
  }else{
    req.flash('danger', 'You have to login first');
    res.redirect('/');
  }
})

router.get('/logout', function(req, res){
  req.logout();
  req.flash('success', 'You are logged out');
  res.redirect('/');
})
module.exports = router;
