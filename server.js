const express = require('express');
const mongoose = require('mongoose');
const databaseConfig = require('./config/database');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const flash = require('connect-flash');
const expressValidator = require('express-validator');
const passport = require('passport');

const app = express();
mongoose.connect(databaseConfig.url);
const db = mongoose.connection;

var port = process.env.PORT || 8080;

db.once('open', function(){
  console.log('Connected to database');
})

db.on('error', function(err){
  console.log(err);
})
//express session middleware
app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}))

//express messages middleware
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

//express validator middleware
app.use(expressValidator());

//body Parser
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

//passport setup
require('./config/passport')(passport);
// Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

//express static serving
app.use(express.static(path.join(__dirname, 'public')));
app.use('/user',express.static(path.join(__dirname, 'public')));
//Pug Load view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//Home Route
app.get('*', function(req, res, next){
  res.locals.user = req.user || null;
  next();
});

app.get('/',function(req, res){
  res.render('index',{title: 'HOME PAGE'});
})

let userRoute = require('./router/user');
app.use('/user', userRoute);

app.listen(port, function(){
  console.log('server Is Running On Port 3000');
})
